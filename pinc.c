#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int i = 0;

static void * thread_start(void *arg) {
	for (; i < 10000; i++) {
		printf("\n");
	}
	return "retval";
}

int main() {
	pthread_t *t, *u;
	char *retval;
	t = malloc(sizeof(pthread_t));
	u = malloc(sizeof(pthread_t));
	pthread_create(t, NULL, &thread_start, retval);
	pthread_create(u, NULL, &thread_start, retval);
	pthread_join(*t, NULL);
	pthread_join(*u, NULL);
	free(t);
	free(u);
	return 0;
}
